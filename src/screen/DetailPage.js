import React, { Component } from 'react';
import { Text,BackHandler, View,ScrollView ,TouchableOpacity , ToastAndroid} from 'react-native';
import CinemakuyAPI from '../library/CinemakuyAPI';
import TimeSchedule from '../library/TimeSchedule';
import { Image ,Icon, Button,  Rating} from 'react-native-elements';
import Style from './style/DetailPageStyle';
import Images from '../library/images';
import Accordion from 'react-native-collapsible/Accordion';
import AsyncStorage from '@react-native-community/async-storage';


class DetailPage extends Component {
	
	constructor(props) {
	  super(props);
	  // Don't call this.setState() here!
	  this.navigate = props.navigation;
	}
	
	static navigationOptions = {
		header: null ,
	}
	
	componentDidMount() {
		//BackHandler.removeEventListener('hardwareBackPress');
		//this.backHandler.remove()
		this.getScheduleData()
	}
	
	getScheduleData(date){
		this.setState({cinemaData : null});
		let data = this.props.navigation.state.params.data
		//console.log(data)
		let isReferNextDay  = false
		let body = { 
					event_id : data.event_id,
					city : this.props.navigation.state.params.city,
					date : date || TimeSchedule.getDate()
					}
		if(data.pre_order && (date==undefined||date==null)){
			body.date = ''
			isReferNextDay = true
		}
		console.log(data.pre_order , (date==undefined||date==null));
		
		
		
		CinemakuyAPI.getMovieSchedule(body , (response)=>{
			console.log("Data Get Finish");
			console.log(response);
			console.log("Current Active ShowDates :");
			let idx = 1;
			if(isReferNextDay){
				console.log(response.data.showdates[1])
			}else{
				idx = -1
				for(let i = 0; i < response.data.showdates.length; i++){
					if(response.data.showdates[i].showdate == body.date){
						idx = i;
						break;
					}
				}
				console.log(response.data.showdates[idx])
			}
			this.setState({cinemaData : response , cinemaIdx : idx , showdates : response.data.showdates});
		});
		
	}
	
	state = {
		currentTab : 0,
		cinemaData : null,
		showdates : [],
		cinemaIdx : 0,
		activeSections: []
	}
	_renderHeader = section => {
		return (
		  <View style={{backgroundColor:'#EEF0EF', marginTop:12 , width:Style.main.screenWidth*0.85, height:50, borderRadius:5,flexDirection: 'row'}}>
			<View style={{backgroundColor:'#00000000',width:Style.main.screenWidth*0.8*0.25, marginLeft:Style.main.screenWidth*0.8*0.05 ,height:50 , padding:5}}>
			<Image 
				source={{uri : section.provider_image }}			
				style={{ width: '100%', height: '90%'}}
			></Image>
			</View>
			<View style={{backgroundColor:'#00000000',width:Style.main.screenWidth*0.8*0.70, height:50 , padding:5 , justifyContent:'center'}}>
				<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,color:'#333333',width:Style.main.screenWidth*0.8*0.7}}>{section.name}</Text>
			</View>
		  </View>
		);
	  };
	
	isTimePassed(time,date){
		console.log(time,date);
		var seperateDate = date.split("-");
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();
		console.log(yyyy,mm,dd,date);
		if(yyyy < seperateDate[0]){
			return false;
		}else if(mm < seperateDate[1]){
			return false;
		}else if(dd < seperateDate[2]){
			return false;
		}
		
		var seperateTime = time.split(":");
		var d = new Date();
		var hour = d.getHours();
		var minute = d.getMinutes();
		if( hour < parseInt(seperateTime[0])){
			return false;
		}else if(hour == parseInt(seperateTime[0]) && minute+30 < parseInt(seperateTime[1]) ){
			return false;
		}else{
			return true;
		}
		
		
	}
	
	pickSeat(schedule_id , date , time,provider_name,provider_image,provider_location,address){
		AsyncStorage.getItem('account/token',(err, result) => {
			if(result != ""){
				this.props.navigation.navigate('PickSeat' , {schedule_id:schedule_id,date:date,time:time,provider:{name:provider_name,image:provider_image,location:provider_location,address:address}});
			}else{
				ToastAndroid.showWithGravity(
				  "You're not logged in",
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
			}
		} )
		
	}
	
	  _renderContent = section => {
		let content = []

		for(let i =0 ; i < section.schedules_list.length; i++){
			content.push(
			<View key={i} style={{backgroundColor:'#283949',margin:15 }}>
				<Text style={{fontFamily:Style.main.fonts.fontOpenSans ,fontSize:12, color:"#FFFFFF" , marginBottom : 10,borderBottomWidth: 1, borderBottomColor: "#FFFFFF"}}>{section.provider_name} - {section.schedules_list[i].schedule_class}</Text>
				<View style={{ flexDirection: 'row',flexWrap: 'wrap', alignItems: 'flex-start'}}>
				{section.schedules_list[i].schedules.map((item,j) =>{
					return(
						<View key={j} style={{marginLeft:'2%' , marginRight:'2%',marginTop : 4 , marginBottom:2}}>
						<Button 
							disabled={item.seat_available!=0||(this.isTimePassed(item.showtime,section.schedules_list[i].showdate))}
							buttonStyle = {{backgroundColor:'#FD6C57',height:20 }}
							  title={item.showtime}
							  titleStyle={{fontFamily:Style.main.fonts.fontOpenSans ,fontSize:10}}
							  onPress={() => {
								  this.pickSeat(item.schedule_id , section.schedules_list[i].showdate ,item.showtime, section.provider_name ,section.provider_image,section.name ,section.address );
								  console.log(item.showtime)
								}}
							></Button>	
						</View>)
				})}
				</View>
				
			 </View>);
		}

		  //<Text style={{color:"#FFFFFF",fontFamily:Style.main.fonts.fontOpenSans ,fontSize:10}} >{item.showtime}</Text>
		return (
		  <View style={{backgroundColor:'#283949' , width : Style.main.screenWidth*0.8-20 , borderBottomLeftRadius:10 , borderBottomRightRadius:10 }}>
			  {content}
		  </View>
		);
	  };

	  _updateSections = activeSections => {
		this.setState({ activeSections });
	  };
	render() {
		
		let data = this.props.navigation.state.params.data;
		//console.log(data);
		let rate ;
		if(data.IMDb_rating=="0"){
			rate = (<Text style={{fontSize: 10,fontFamily:Style.main.fonts.fontOpenSans,color:'#d0d0d0',marginTop:'1%'}}>(Unrated)</Text>);
		}else{
			rate = (<Rating
							  imageSize={15}
							  readonly
							  startingValue={data.IMDb_rating/2}
							/>)
		}
		let day = []
		//Render Day
		if(this.state.currentTab == 0){
			//console.log(Style.styleDayActive)
			for(let i=0; i < this.state.showdates.length ; i++){
				day.push(<View key={i} style={{backgroundColor:'#FFFFFF00',borderWidth:1,borderColor:'#FFFFFF',marginLeft : 10,marginRight : 10,height:50,width:50,borderRadius:5,justifyContent:'center' , alignItems:'center'}}>
							<TouchableOpacity 
										onPress={()=>{console.log("Date Clicked : ",i); this.getScheduleData(this.state.showdates[i].showdate); }} 
										underLayColor='#FFFFFF'
										style={{ position:'absolute',width : '100%' , height : '100%',justifyContent:'center' , alignItems:'center'}}
										>
							<View style={{justifyContent:'center' , alignItems:'center',borderWidth : 1,borderColor:'#FFFFFF' , borderTopRightRadius: 5 ,borderTopLeftRadius: 5, height:'50%' , width:'100%'}}>
								<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,color:'#FFFFFF'}}> {this.state.showdates[i].date} </Text>
							</View>
							<View style={ this.state.cinemaIdx==i ? Style.styleSheets.styleDayActive : Style.styleSheets.styleDayUnactive}>
								{this.state.showdates[i].day != 'Today' &&
								<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,color:'#FFFFFF'}}> {this.state.showdates[i].day.toUpperCase()} </Text>}
								{this.state.showdates[i].day == 'Today' &&
								<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,color:'#FFFFFF'}}>NOW</Text>}
							</View>
								
							
							</TouchableOpacity>
						 </View>);
			}
		}else{
			
		}
		
		
		return (
		  <View style={{ justifyContent: "center", alignItems: "center"   }}>
			<View style={{ position:'absolute', top : 0,height : Style.main.screenHeight , width : Style.main.screenWidth , backgroundColor:'#2D3E50' }}>
			</View>
			<View style={{ position:'absolute', top : 0,  width : Style.main.screenWidth ,justifyContent: "center", alignItems: "center" , height:(Style.main.screenHeight/4)*2,backgroundColor:'#2D3E50' }}>
				<View style={{
						position:'absolute',
						top:'0%',
						flex:1,
						width:'100%',
						height:(Style.main.screenHeight/4)*2,
						overflow: 'hidden'
				}}>
				
					<Image 
							source={{uri : data.image }}
							style={{ width: '100%', height: '100%',marginTop:Style.main.screenHeight/10}}
						>
					</Image>
					
				</View>
				<View style={{
						position:'absolute',
						top:'0%',
						flex:1,
						width:'100%',
						height:(Style.main.screenHeight/4)*2,
						overflow: 'hidden'
				}}>
					<Image 
						source={Images.layerFade}
						style={{ width: '100%', height: '100%'}}
					>
					</Image>
				</View>
			</View>
			
			<ScrollView style={{ backgroundColor: 'transparent',margin:0 , padding:0 , width : Style.main.screenWidth }}>
				
				<View style={{ backgroundColor: '#2D3E50',justifyContent: "center", alignItems: "center" ,overflow: 'visible', marginTop : (Style.main.screenHeight/4)*2 }}>
					{/*Header Detail*/}
					<View style={Style.styleSheets.headerDetail}>
						<View style={{width: Style.main.screenWidth/4.5, height: '140%',marginTop:'-10%',marginLeft:'7%'}}>
							<Image
									source={{uri : data.image }}
									style={{width:'100%', height: '100%'}}
								>
							</Image>
						</View>
						<View style={{marginLeft:'5%',marginTop:'2%' ,textAlign:"left"}}>
							<Text style={{color:"#EBEDEC",width:Style.main.screenWidth*0.6,fontSize: 16,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans}}>{data.name}</Text>
							<View style={{flexDirection: 'row',color:"#394557"}}>
								<View >
									<Text style={{fontSize: 10,fontFamily:Style.main.fonts.fontOpenSans,color:'#8f8f8f'}}>Director</Text>
									<Text style={{fontSize: 10,fontFamily:Style.main.fonts.fontOpenSans,color:'#8f8f8f'}}>Duration</Text>
									<Text style={{fontSize: 10,fontFamily:Style.main.fonts.fontOpenSans,color:'#8f8f8f'}}>Rating</Text>
								</View>
								<View style={{marginLeft : 12}}>
									<Text style={{fontSize: 10,fontFamily:Style.main.fonts.fontOpenSans,color:'#d0d0d0'}}>{data.director}</Text>
									<Text style={{fontSize: 10,fontFamily:Style.main.fonts.fontOpenSans,color:'#d0d0d0'}}>{data.duration}</Text>
									{rate}
								</View>
							</View>
		
						</View>
					</View>
					
					{/*Tab*/}
					<View style={{backgroundColor:'#2D3E50',paddingLeft:'5%',flexDirection: 'row',justifyContent: 'flex-start',width:Style.main.screenWidth,height:Style.main.screenHeight*0.05}}>
						<Button 
						  buttonStyle={this.state.currentTab==0?Style.styleSheets.styleActiveTab:Style.styleSheets.styleUnactiveTab}
						  title="Date" 
						  titleStyle={this.state.currentTab==0?Style.styleSheets.styleActiveTitle:Style.styleSheets.styleUnactiveTitle}
						  onPress={() => {this.setState({currentTab : 0})}}
						></Button>
						<Button 
						  buttonStyle={this.state.currentTab==1?Style.styleSheets.styleActiveTab:Style.styleSheets.styleUnactiveTab}
						  title="Story" 
						  titleStyle={this.state.currentTab==1?Style.styleSheets.styleActiveTitle:Style.styleSheets.styleUnactiveTitle}
						  onPress={() => {this.setState({currentTab : 1})}}
						></Button>
					</View>
					{/*Movie Corosel*/}
					{this.state.currentTab == 0?
						<View style={{width:Style.main.screenWidth ,backgroundColor:'#2D3E50',marginTop:10 ,justifyContent: "center", alignItems: "center" }}>

							
							<View style={{backgroundColor:'#283949' , overflow:'hidden' , marginTop:10,marginBottom:10,width:Style.main.screenWidth*0.85, height:Style.main.screenHeight*0.1, borderRadius:5,justifyContent: "center",alignItems: "center"}}>
								<ScrollView  contentContainerStyle={{flexGrow: 1}} horizontal={true} style={{backgroundColor:'#283949',height:'90%',paddingTop:'2%'}}>
									{day}
								</ScrollView>
							</View>
						
							
							{this.state.cinemaData!= null ? <Accordion
								sections={this.state.cinemaData.data.cinemas}
								touchableComponent={TouchableOpacity}
								activeSections={this.state.activeSections}
								renderHeader={this._renderHeader}
								renderContent={this._renderContent}
								onChange={this._updateSections}
								style={{justifyContent: "center", alignItems: "center" }}
							/>:
							<View style={{backgroundColor:'#283949' , overflow:'hidden' , marginTop:10,marginBottom:10,width:Style.main.screenWidth*0.85, borderRadius:5,padding:10}}>
								
								<Text style={{color:"#FFFFFF",fontFamily:Style.main.fonts.fontOpenSans,fontSize:12}}>Loading...</Text>
								
							</View>}
						
						</View>
						:
						<View style={{marginTop:30}}>
							<Text style={{color:"#FFFFFF",fontFamily:Style.main.fonts.fontOpenSans,fontSize:14}}>Actors</Text>
							<View style={{backgroundColor:'#283949' , overflow:'hidden' , marginTop:10,marginBottom:10,width:Style.main.screenWidth*0.85, height:Style.main.screenHeight*0.05, borderRadius:5,justifyContent: "center",alignItems: "center"}}>
								<ScrollView  contentContainerStyle={{flexGrow: 1}} horizontal={true} style={{backgroundColor:'#283949',height:'90%',paddingTop:'2%'}}>
									<View style={{justifyContent: "center",height:'68%',paddingLeft:10,paddingRight:10}}>
										<Text style={{color:"#FFFFFF",fontFamily:Style.main.fonts.fontOpenSans,fontSize:12}}>{this.props.navigation.state.params.data.actor}</Text>
									</View>
								</ScrollView>
							</View>
							<Text style={{color:"#FFFFFF",fontFamily:Style.main.fonts.fontOpenSans,fontSize:14}}>Synopsis</Text>
							<View style={{backgroundColor:'#283949' , overflow:'hidden' , marginTop:10,marginBottom:10,width:Style.main.screenWidth*0.85, borderRadius:5,padding:10}}>
								
								<Text style={{color:"#FFFFFF",fontFamily:Style.main.fonts.fontOpenSans,fontSize:12}}>{this.props.navigation.state.params.data.synopsis}</Text>
								
							</View>
						</View>
						
					}
					
				</View>
				
				<View style={{height : Style.main.screenHeight*0.05 , width:Style.main.screenWidth}}>
				</View>
			</ScrollView >
			
		  </View>
		);
	}
}

export default DetailPage