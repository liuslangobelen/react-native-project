import React, { Component, Fragment } from 'react';
import { Alert, Text, View,ImageBackground, Image,StatusBar} from 'react-native';
import { Button, Input, Icon} from 'react-native-elements';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Images from '../library/images';
import CinemakuyAPI from '../library/CinemakuyAPI';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';
import Style from './style/RegisterPageStyle';


class EditProfilePage extends Component {
	constructor(){
		super();
		this.UpdatePassProcessed = this.UpdatePassProcessed.bind(this);
	}
	static navigationOptions = {
		header: null ,
	}
	
	state = {
		oldpass:"",
		newpass:"",
		cnewpass:"",
		loading:false,
		udata:null
    }
	
	getAccount(username,token){
		console.log("masuk1");
		CinemakuyAPI.GetAccount({username,token} , (response)=>{
			console.log("Data Get Finish");
			console.log(response);
			this.setState({udata : response});
			
		});
	}
	componentDidMount() {
		AsyncStorage.getItem('account/username',(err, result) => {
			username = result;
			AsyncStorage.getItem('account/token',(err, result) => {
				token = result;
				this.getAccount(username , token);
			} )
		} )
	}
	
	Validate(val){
		
		if(val.cnewpass == "" || val.newpass == "" || val.oldpass == ""){
			return "Please fill all fields"
		}else if(val.oldpass.length < 6 || val.cnewpass < 6){
			return "Password should have at least 6 character"
		}else if(val.newpass!=val.cnewpass){
			return "new password doesnt match"
		}
		
		return ""
	}
	
	
	
	CinemakuyUpdatePassProcess(){
		
		let validateResult = this.Validate(this.state);
		if(validateResult != ""){
			Alert.alert("Invalid Input" , validateResult);
			return
		}
		this.setState({loading:true});
		
		let request = {oldpass : this.state.oldpass ,username : username, newpass : this.state.newpass , 
						cnewpass : this.state.cnewpass, token:token}

		console.log("req : ",request);
		
		CinemakuyAPI.ChangePass(request , this.UpdatePassProcessed );
		
	}
	
	UpdatePassProcessed(response){
		console.log(response);
		if(response.isSuccess){
			
			ToastAndroid.showWithGravity(
			  'Password successfully changed',
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
			this.props.navigation.navigate('Home');
		}else{
			
			ToastAndroid.showWithGravity(
			  'Sorry : '+response.respon,
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
			
			this.setState({loading:false})
		}
	}
	
	render() {
		
		return (
			
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<StatusBar
					backgroundColor='#313C4C'
				/>
				<View style={Style.styleSheets.formContainer} >
					<ImageBackground
					  source={Images.registerBg}
					  style={Style.styleSheets.fullScreen}
					/> 
				</View>
				<View style={Style.styleSheets.formContainer} >
				
					<View style={{width:'100%',alignItems: 'center',}}>
					
					<Text style={{color:'#ffffff',fontFamily:Style.main.fonts.fontElegance,fontSize:30,marginTop:20,letterSpacing:5}}>Change Password</Text>
					
					<Input
						  placeholder='Username'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  editable={false}	
						  value={username}
						  leftIcon={
							<Icon
							  name='person-pin'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					<Input
					  placeholder='Old Password'
					  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
					  containerStyle={{ width:'68%',marginBottom:15}}
					  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
					  placeholderTextColor='#FD6C57'
					  onChangeText={(value) => { this.setState({oldpass:value}) }}
					  secureTextEntry={true} 
											  
					  leftIcon={
						<Icon
						  name='lock'
						  size={20}
						  color='#FD6C57'
						  style={{marginLeft:-10}}
						/>
					  }
					/>	
				
					<Input
						  placeholder='New Password'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({newpass:value}) }}
						  secureTextEntry={true} 
						  						  
						  leftIcon={
							<Icon
							  name='lock'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>			
					<Input
						  placeholder='Confirm new Password'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({cnewpass:value}) }}
						  secureTextEntry={true} 
						  						  
						  leftIcon={
							<Icon
							  name='lock'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					<Button 
					  buttonStyle={Style.styleSheets.loginButton}
					  onPress={() => {this.CinemakuyUpdatePassProcess();}}
					  loading={this.state.loading}
					  title="CONFIRM" titleStyle={{color:'#FD6C57',fontFamily:Style.main.fonts.fontElegance}}
					  disabled={this.state.loading}
					></Button>
					
					
				</ View>
					
			</View>
		</View>
			
		);
	}
}

export default EditProfilePage