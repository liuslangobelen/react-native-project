import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View,ImageBackground,Alert,Image,TouchableOpacity,ToastAndroid} from 'react-native';
import Button from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { StackNavigator } from 'react-navigation';
import Images from '../../../library/images';
import CinemakuyAPI from '../../../library/CinemakuyAPI';
import Style from '../../style/MainStyle'

class SideMenu extends Component {

  
  getAccount(username,token){
		//console.log("masuk1");
		CinemakuyAPI.GetAccount({username,token} , (response)=>{
			console.log("Data Get Finish");
			console.log(response);
			this.setState({udata : response});
			this.setState({
					token : token,
					username : username,
					name : response.name,
					phone : response.phone,
					credit : response.credit
				});
		});
	}
	componentDidMount() {
		// AsyncStorage.getItem('account/username',(err, result) => {
			// username = result;
			// AsyncStorage.getItem('account/token',(err, result) => {
				// token = result;
				// this.getAccount(username , token);
			// } )
		// } )
		// AsyncStorage.multiGet(['account/token','account/username'], (err, result) => {
			// this.getAccount(result[1][1],result[0][1]);
		// });
	}
  
  LogoutCheck(){
	  Alert.alert("Konfirmasi Log out","Yakin akan keluar dari akun ini?",
				[
					{text: 'Ya', onPress: () => {	console.log('Ya pressed'); 
													this.resetDataAccount(); 
													console.log(this.props.nav)
													this.props.nav.navigate('Login');
												},style: 'cancel'},
					{text: 'Tidak', onPress: () => {console.log('OK Pressed'); }},
				],
				{cancelable: true}
			);
			return true;
  }
  resetDataAccount(){
		AsyncStorage.setItem('account/token', "");
		AsyncStorage.setItem('account/username', "");
	}
	


	CinemakuyLoginChecking(username, token){
		let request = {username : username , token : token}
		CinemakuyAPI.LoginCheck(request , (isSuccess)=>{
			if(isSuccess){
				
				ToastAndroid.showWithGravity(
				  'Kamu sudah terverivikasi masuk',
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
				console.log("already loginnn");
			}
		})	
	}
	moneyToCurency(money) {
		money = money+"";
		let extra = money.includes("-")?1:0;
		pointCount = (money.length-1-extra) / 3;
		let panjang = money.length;
		for(let i = 1 ; i <= pointCount; i++){
			let len = panjang - (3*i);
			money = money.slice(0, len) + "." + money.slice(len);
		}

		return money;
	}
	
  render () {
	  	let name,phone,credit;

	/*  AsyncStorage.multiGet(['account/token','account/username'], (err, result) => {

			if(result[0][1] != "" && result[1][1]!=""){
				console.log("Check Account Login Status");
				this.CinemakuyLoginChecking(result[1][1],result[0][1]);
				  alreadyLogin=(<Text>"sudah login"</Text> )
				  				console.log(alreadyLogin);

			}else alreadyLogin = (<Text>"beluum login"</Text>)
		});
	*/

		let username = "";
		let token = "";
		
		//console.log('tes')
		//console.log(this.props.udata);
		if(this.props.udata!=null && this.props.udata!=""){
			//console.log(this.state.udata);
			name=this.props.udata.name;
			phone=this.props.udata.phone;
			credit=this.props.udata.credit;
		}
		
				  				//console.log(alreadyLogin);
	
    return (
      <View style={{backgroundColor:'#ECF0F1'}}>
		
		
		<View style={{height:'25%',marginTop:'0%'}}>
			
			<View>
				<ImageBackground
				 source={Images.profileBg}
				style={{widht:'100%', height:'100%'}}
				/>
			</View>
			<View style={{paddingLeft:'12%',marginTop:'-35%'}}>
				
				<View style={{flexDirection : 'row',alignItems:'center'}}>
					
					<Text style={{color:'white',fontSize:22,fontWeight:'bold',width : Style.screenWidth*(5/7)}}>
						{this.props.userAccountData.name}
					</Text>
					
				</View>
				<Text style={{color:'#d7dbd9',fontSize:13,marginTop:'0.5%',marginLeft:'1%'}}>
					{this.props.userAccountData.phone}
				</Text>
				
				<View style={{marginTop:'3%'}}>
					<View style={{flexDirection:'row'}}>
						<View style={{width:'70%'}}>
							<Text style={{color:'white',fontSize:16}}>
								Rp. {this.moneyToCurency(this.props.userAccountData.credit)},-
							</Text>
						</View>
						<TouchableOpacity onPress={()=>{console.log("edit clicked");this.props.nav.navigate('TopUp' , {udata:this.props.udata , username:this.props.userAccountData.username});}} style={{width:'25%'}}>
							<View style={{flexDirection:'row',alignItems:'center'}}>
								<Icon
									name='plus'
									color='#313131'
									iconStyle={{marginLeft:100}}
								/>
								<Text style={{color:'white'}}>Top Up</Text>
							</View>
						</TouchableOpacity>
						<View style={{width:'15%'}}></View>
					</View>
					
					

				</View>
			</View>
			
		</View>
		

		<View style={{ marginLeft:'-30%',marginTop:'10%',backgroundColor:'#ecf0f1',height:'55%'}}>
				
				<TouchableOpacity onPress={()=>{console.log("Ticket Clicked");this.props.nav.navigate('Tickets');}}>
					<View style={{flexDirection : 'row',alignItems:'center'}}>
						<Image
							source={Images.tiket}
							style={{flex : 1,height : 45, width: 45,resizeMode : 'contain',marginLeft:'6%'}}
						/>
						<Text></Text>
						<Text style={{fontSize:18,flex:1, marginLeft:'-12%',fontWeight:'bold',color:'#313131'}}>
							Tickets
						</Text>
						
					</View>
				</TouchableOpacity>
				
				<TouchableOpacity onPress={()=>{console.log("Payment Order Clicked");this.props.nav.navigate('PaymentOrder' , {username : this.props.userAccountData.username , token : this.props.userAccountData.token});}}>
					<View style={{flexDirection : 'row',alignItems:'center'}}>
						<Image
							source={Images.payment}
							style={{flex : 1,height : 45, width: 45,resizeMode : 'contain',marginLeft:'6%'}}
						/>
						<Text></Text>
						<Text style={{fontSize:18,flex:1, marginLeft:'-12%',fontWeight:'bold',color:'#313131'}}>
							Payment Orders
						</Text>
						
					</View>
				</TouchableOpacity>
				
				<TouchableOpacity onPress={()=>{console.log("Change Pass clicked");this.props.nav.navigate('ChangePass');}}>
					<View style={{flexDirection : 'row',alignItems:'center',marginTop:'1%'}} >
						<Image
							source={Images.set}
							style={{flex : 1,height : 35, width: 35,resizeMode : 'contain',marginLeft:'6%'}}
						/>
						<Text style={{fontSize:18,flex:1, marginLeft:'-12%',fontWeight:'bold',color:'#313131'}}>
							Change Password
						</Text>
							
					</View>
				</TouchableOpacity>
				<TouchableOpacity onPress={()=>{console.log("edit clicked");}}>
					<View style={{flexDirection : 'row',alignItems:'center',marginTop:'2%'}}>
						<Image
							source={Images.about}
							style={{flex : 1,height : 35, width: 35,resizeMode : 'contain',marginLeft:'6%'}}
						/>
						<Text style={{fontSize:18,flex:1, marginLeft:'-12%',fontWeight:'bold',color:'#313131'}}>
							About Us
						</Text>
						
					
					</View>
				</ TouchableOpacity>
				
				
		</View>
		<View style={{backgroundColor:'#ecf0f1',height:'20%',alignItems:'center'}}>

			
			<TouchableOpacity onPress={()=>{console.log("logout clicked");this.LogoutCheck();}}>
				<View style={{borderColor: '#ecf0f1',backgroundColor:'#FF6A52',borderRadius:20,padding:10,width:110, alignItems: "center"}}>
					<Text style={{fontSize:18,color:'#ECF0F1',fontWeight:'bold'}}>
						Logout
					</Text>
				</View>
			</TouchableOpacity>
			
		</View>
		
		<View style={{position:'absolute' , top:10 , right:10}}>
			<TouchableOpacity onPress={()=>{console.log("edit clicked");this.props.nav.navigate('EditProfile');}}>
				<View>
					<Icon
							name='gear'
							color='white'
							size={20}
							style={{marginLeft:10}}
					/>
				</View>

			</TouchableOpacity>
		</View>
		
	  </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;