import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';
import { StackNavigator } from 'react-navigation';
import { Icon,Button} from 'react-native-elements';
import {Text, View,ImageBackground,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Images from '../../../library/images';
import Style from '../../style/HomePageStyle';


class GuestSideMenu extends Component {
	
	
	
	render(){
		
		/*<ImageBackground
					 source={Images.profileBg}
					style={{width:'50%', height:'50%'}}
					/>*/
		
		return(
			<View>
			
				<View style={{position:'absolute',width:Style.main.screenWidth*(6/7),height:Style.main.screenWidth*(6/7)*0.6,backgroundColor:'#000000'}}>
					<ImageBackground
					 source={Images.profileBg}
					style={{width:'100%', height:'100%'}}
					/>
				</View>
				<View style={{blur:'70%',height:'100%',alignItems:'center',justifyContent:'center'}}>
					
						<TouchableOpacity onPress={()=>{console.log("login clicked");this.props.nav.navigate('Login');}}>
							<View style={{borderColor: '#ecf0f1',backgroundColor:'#FF6A52',borderRadius:20,padding:10,width:110, alignItems: "center"}}>
								<Text style={{fontSize:18,color:'#ECF0F1',fontWeight:'bold'}}>
									Login
								</Text>
							</View>
						</TouchableOpacity>
				</View>
				
			
			</View>
		);
	}
	
	
}

GuestSideMenu.propTypes = {
  navigation: PropTypes.object
};

export default GuestSideMenu;