import React, { Component } from 'react';
import {Text , View,TouchableOpacity} from 'react-native';
import { Image } from 'react-native-elements';
import Styles from '../../style/MainStyle'


class ListMovie extends Component {
	constructor(){
		super();
		this.onClickedButton = this.onClickedButton.bind(this);
	}
	
	onClickedButton(){
		this.props.onPress(this.props.data)
	}
	
	render() {
		return (
				
					<View style={{ 	width : '90%' , height : 110 , 
									backgroundColor:	'#35536F' , borderRadius: 8 , 
									margin : 10, flex:1, flexDirection: 'row' }}
									>
						
						<View style={{flex:0.3}}>
							<View style={{marginLeft:'10%',marginTop:'-10%',width: '75%', height: '102%',backgroundColor:'#35000000'}}>
								<Image 
									source={{uri : this.props.data.image}}
									style={{width: '100%', height: '100%'}}
								/>
							</View>
						</View>
						<View style={{flex:0.7}}>
							<Text style={{marginTop:'4%',marginRight:'20%' , color:'#ffffff' , fontFamily:Styles.fonts.fontOpenSans,fontSize:15,fontWeight: 'bold'}}>
							{this.props.data.name}
							</Text>
							<Text style={{position:'absolute', marginTop:'5%' , color:'#ffffff' ,marginLeft:'83%', fontFamily:Styles.fonts.fontOpenSans,fontSize:10,fontWeight: 'bold'}}>
							({this.props.data.duration.replace('inutes', '')})
							</Text>
							<View style={{flexDirection: 'row',margin:3}}>
								<View style={{borderColor:'#FD6C57',borderWidth:1,borderRadius:15,alignItems:'center',paddingLeft:3,paddingRight:3,
												height:'100%'}}>
									<Text style={{fontSize:10,fontFamily:Styles.fonts.fontOpenSans,color:'#FD6C57',}}>
										{this.props.data.rating}
									</Text>
								</View>
								
								<View style={{borderColor:'#40C2AA',borderWidth:1,borderRadius:15,alignItems:'center',paddingLeft:3,paddingRight:3,
												height:'100%',marginLeft:10}}>
									<Text style={{fontSize:10,fontFamily:Styles.fonts.fontOpenSans,color:'#40C2AA',}}>
										{this.props.data.genre.toUpperCase()}
									</Text>
								</View>
								
							</View>
							<View style={{flexDirection: 'row'}}>
								<Text style={{fontSize:10,fontFamily:Styles.fonts.fontOpenSans,color:'#8f8f8f'}} >Director : </Text>
								<Text style={{fontSize:10,fontFamily:Styles.fonts.fontOpenSans,color:'#d0d0d0'}}>
								{this.props.data.director}
								</Text>
							</View>
						  
						</View>
						
						<TouchableOpacity 
									onPress={this.onClickedButton} 
									underLayColor='#FFFFFF'
									style={{ position:'absolute',width : '100%' , height : '100%'}}
									>
									<Text></Text>
						</TouchableOpacity>
					</View>
				
			
		);
	}
}

export default ListMovie