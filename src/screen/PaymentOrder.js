import React, { Component } from 'react';
import { Text, View,TouchableOpacity , ScrollView ,ToastAndroid} from 'react-native';
import CinemakuyAPI from '../library/CinemakuyAPI';
import Accordion from 'react-native-collapsible/Accordion';
import Style from './style/DetailPageStyle';
import { Image , Button} from 'react-native-elements';

class PaymentOrder extends Component {
	
	
	constructor(props) {
	  super(props);
	  // Don't call this.setState() here!
	  this.loadData = this.loadData.bind(this);
	}
	
	static navigationOptions = {
		header: null ,
	}
	componentDidMount() {
		
		this.loadData();
	}
	
	loadData(){
		let body = {
			username : this.props.navigation.state.params.username,
			token : this.props.navigation.state.params.token
		}
		CinemakuyAPI.GetUserPaymentOrders( body , (orders)=>{
			let i = 0 ;
			let arrayOrders = []
			for( let key in orders.data){
				arrayOrders.push(orders.data[key]);
				arrayOrders[i].key = key;
				i++;
			}
			
			arrayOrders.sort(function(x, y) {
			  if (x.expired < y.expired) {
				return 1;
			  }
			  if (x.expired > y.expired) {
				return -1;
			  }
			  return 0;
			});
			
			console.log(arrayOrders)

			this.setState({orders : arrayOrders});
		});
	}
	
	deleteOrders(key){
		let body = {
			username : this.props.navigation.state.params.username,
			token : this.props.navigation.state.params.token,
			key : key
		}
		console.log(key);
		CinemakuyAPI.DeleteUserPaymentOrders(body , (response)=>{
			console.log(response);
			this.loadData();
			ToastAndroid.showWithGravity(
			  response.respon,
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
		});
		
		
	}
	state = {
		orders : [],
		activeSections: []
	}
	
	_updateSections = activeSections => {
		this.setState({ activeSections });
	};
	
	isDatePassed(dateX){
		// -1 passed , 1 not passed yet
		let date = dateX.split(' ');
		
		var seperateDate = date[0].split("-");
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();
		if(yyyy < seperateDate[0]){
			return true;
		}else if(mm < seperateDate[1]){
			return true;
		}else if(dd < seperateDate[2]){
			return true;
		}
		if(yyyy > seperateDate[0]){
			return false;
		}else if(mm > seperateDate[1]){
			return false;
		}else if(dd > seperateDate[2]){
			return false;
		}
		
		var seperateTime = date[1].split(':');
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getSeconds();
		
		if(h < seperateTime[0]){
			return true;
		}else if(m < seperateTime[1]){
			return true;
		}else if(s < seperateTime[2]){
			return true;
		}
		
		return false;
	}
	moneyToCurency(money) {
		money = money+"";
		let extra = money.includes("-")?1:0;
		pointCount = (money.length-1-extra) / 3;
		let panjang = money.length;
		for(let i = 1 ; i <= pointCount; i++){
			let len = panjang - (3*i);
			money = money.slice(0, len) + "." + money.slice(len);
		}

		return money;
	}
	_renderHeader = section => { 
		let colorDate = '#FD6C57';
		if(this.isDatePassed(section.expired) && section.tstatus != 0){
			colorDate = '#58db7b';
		}
		
		
		return (
		  <View style={{ marginTop:12 , width:Style.main.screenWidth*0.90, height:50,borderBottomWidth:1 ,borderColor:'#BBBBBB',flexDirection:'row' }}>
				<View style={{width:Style.main.screenWidth*0.90*0.5 , justifyContent:'center'}}>
					<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333',width:Style.main.screenWidth*0.80*0.6}}>Rp. {this.moneyToCurency(section.count)}</Text>
				</View>
				<View style={{width:Style.main.screenWidth*0.90*0.5 , justifyContent:'center',alignItems:'center'}}>
					<View style={{backgroundColor : colorDate  , borderRadius : 5 , padding : 5 , justifyContent:'center',alignItems:'center'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:10,color:'#FFFFFF'}}>{section.expired}</Text>
					</View>
				</View>
				
		  </View>
		);
	}
	
	_renderContent = section => {
		let status = ''
		switch(section.tstatus){
			case -1 :
				status = 'Pending'
			break;
			case 0 :
				status = 'Failed'
			break;
			case 1 : 
				status = 'Success'
			break;
		}
		if(!this.isDatePassed(section.expired)){
			if(status == 'Pending'){
				status = 'Failed';
			}
			return (
				<View style={{width : '90%'}}>
					<View style={{width:'100%' , flexDirection:'row'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:18, fontWeight : 'bold',color:'#333333' , width:'40%'}}>Expired</Text>
					</View>
					<View style={{width:'100%' , flexDirection:'row'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%'}}>Status</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: {status}</Text>
					</View>
					<View style={{width:'100%' , flexDirection:'row' , marginBottom : 20}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%'}}>Amount</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: Rp. {this.moneyToCurency(section.count)}</Text>
					</View>
				</View>
			);
		}else{
			return (
				<View style={{width : '90%'}}>
					{section.tstatus == 0 &&
					<View style={{width:'100%' , flexDirection:'row'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:18, fontWeight : 'bold',color:'#333333' , width:'40%'}}>Canceled</Text>
					</View>}
				
					<View style={{width:'100%' , flexDirection:'row'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%' , fontWeight : 'bold'}}>Name</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: {section.tname}</Text>
					</View>
					<View style={{width:'100%' , flexDirection:'row'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%', fontWeight : 'bold'}}>Created</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: {section.date} {section.expired.split(' ')[1]} WIB</Text>
					</View>
					<View style={{width:'100%' , flexDirection:'row'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%', fontWeight : 'bold'}}>Status</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: {status}</Text>
					</View>
					<View style={{width:'100%' , flexDirection:'row'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%', fontWeight : 'bold'}}>Provider</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: {section.via}</Text>
					</View>
					{section.tstatus != 0 &&
						<View style={{width:'100%' , flexDirection:'row'}}>
							<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%', fontWeight : 'bold'}}>Payment Code</Text>
							<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: {section.ref}</Text>
						</View>
					}
					<View style={{width:'100%' , flexDirection:'row' , marginBottom : 20}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , width:'40%', fontWeight : 'bold'}}>Amount</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333', width:'60%'}}>: Rp. {this.moneyToCurency(section.count)}</Text>
					</View>
					{section.tstatus != 0 &&
					<View style={{flexDirection:'row' , width:'100%'}}>
						<View style={{width:'70%'}}>
							<Button
								buttonStyle={{width:'100%'}}
								title="<< More Details"
								onPress={()=>{
									console.log('its')
									this.goToDetails(section);
								}}
							></Button>
						</View>
						<View style={{width:'25%', marginLeft:'5%'}}>
						<Button
							buttonStyle={{width:'100%',  backgroundColor:'#FD6C57' }}
							title="Cancel"
							onPress={()=>{
								console.log('its')
								this.deleteOrders(section.key);
							}}
						></Button>
						</View>

					</View>
					}
				</View>
			);
		}
	}
	
	goToDetails(data){
		console.log('clicked')
		this.props.navigation.navigate('PaymentOrderDetails' , data);
	}
	
	render() {
		
		
		return (
		  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
			<ScrollView>
				<View style={{justifyContent:'center' , alignItems:'center' , width : Style.main.screenWidth,marginTop:30 , marginBottom : 30}}>
					<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:18,fontWeight:'bold',color:'#333333'}}>Your Payment Orders</Text>
				</View>
				<View style={{justifyContent:'center' , alignItems:'center' , width : Style.main.screenWidth,marginBottom : 30}}>
					<Accordion
						sections={this.state.orders }
						touchableComponent={TouchableOpacity}
						activeSections={this.state.activeSections}
						renderHeader={this._renderHeader}
						renderContent={this._renderContent}
						onChange={this._updateSections}
						style={{justifyContent: "center", alignItems: "center" }}
					/>
				</View>
			</ScrollView>
		  </View>
		);
	}
}

export default PaymentOrder